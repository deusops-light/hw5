#!/bin/bash

# sudo apt-get install -y \
#     ca-certificates \
#     curl \
#     gnupg \
#     lsb-release > /dev/null

# sudo mkdir -p /etc/apt/keyrings
# curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
# echo \
#     "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
#     $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# apt-get update > /dev/null
# apt-get install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin > /dev/null
# gpasswd -M root,vagrant docker

mkdir -p /opt/sample-app && git clone https://gitfront.io/r/deusops/Fsjok1dx89xG/flask-project-01.git /opt/sample-app

sudo apt-get install -y libpq-dev gcc python3.8 python3-pip python3.8-venv > /dev/null
# python3-flask

sed -i "s/Flask-SQLAlchemy==2.4.4/Flask-SQLAlchemy==2.4.4\njinja2==3.0.3\nitsdangerous==2.0.1\nwerkzeug==2.0.3/g" /opt/sample-app/requirements.txt
cd /opt/sample-app
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt && pip install -r requirements-server.txt
echo $APP_PORT
flask db upgrade && gunicorn app:app -b 0.0.0.0:$APP_PORT --daemon
ufw allow $APP_PORT