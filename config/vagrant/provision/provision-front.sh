#!/bin/bash
echo "+------------------+-----------+"
echo -e "\e[34m Устанавливаем nginx \e[0m"
apt install nginx -y > /dev/null
systemctl enable nginx && systemctl start nginx
ufw allow 'Nginx HTTP'
echo "+------------------+-----------+"

cp /vagrant/config/nginx/sample-app.conf /etc/nginx/sites-available/sample-app.conf
unlink /etc/nginx/sites-enabled/default
ln -s /etc/nginx/sites-available/sample-app.conf /etc/nginx/sites-enabled/sample-app.conf
sed -i "s/\$BACKEND_IP_ADDR/$BACKEND_IP_ADDR/g" /etc/nginx/sites-available/sample-app.conf
sed -i "s/\$BACKEND_PORT/$BACKEND_PORT/g" /etc/nginx/sites-available/sample-app.conf
cat /etc/nginx/sites-available/sample-app.conf
sudo nginx -t >/dev/null 2>&1
if [ $? -eq 0 ]; then
    nginx -s reload
else
    sudo nginx -t
fi